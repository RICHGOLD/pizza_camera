package com.richgold.pizza_camera;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends Activity {
    private View layout;
    private Camera camera;
    private SurfaceHolder mHolder;
    private final static String TAG = "RICHGOLD";//MainActivity.class.getSimpleName();

    private final static int BACK_CAM_ID = 0;
    private final static int FRONT_CAM_ID = 1;
    private final static int EXT_CAM_ID = 2;
    private static final int REQUEST_NORMAL = 10000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set full screen, no title and status bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        layout = this.findViewById(R.id.buttonlayout);

        SurfaceView surfaceView = (SurfaceView) this.findViewById(R.id.surfaceView);
        mHolder = surfaceView.getHolder();
        surfaceView.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        surfaceView.getHolder().setFixedSize(176,144);
        surfaceView.getHolder().setKeepScreenOn(true);
        surfaceView.getHolder().addCallback(new SurfaceCallback());

        // check permission
        if (checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "#onCreate() - request permission");
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_NORMAL);
        } else {
            Log.d(TAG, "#onCreate() - granted");
            surfaceView.setVisibility(View.VISIBLE);
        }

        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "#onCreate() - request permission");
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_NORMAL);
        } else {
            Log.d(TAG, "#onCreate() - granted");
            surfaceView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_NORMAL) {
            boolean anyDenied = Arrays.stream(grantResults).anyMatch(r -> r == PackageManager.PERMISSION_DENIED);
            if (!anyDenied) {
                Log.d(TAG, "#onRequestPermissionsResult() - granted");
            }
        }
    }

    public void back_cam_power() {
        Runtime runtime = Runtime.getRuntime();
        Process process;
        String res = "-0-";
        try {
            String cmd = "ha_command 1";
            process = runtime.exec(cmd);
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;

            while ((line = br.readLine()) != null)  {
                Log.i("send ha command", line);
            }

        } catch (Exception e) {
            e.fillInStackTrace();
            Log.e("send_ha_command", "un execute command");
        }

    }

    public void create_camera(int camera_id, String sensor_format)
    {
        try {
            camera = Camera.open(camera_id);
            Camera.Parameters parameters = camera.getParameters();
            Log.i(TAG, parameters.flatten());
                /*parameters.setPreviewSize(960,480);
                parameters.setPreviewFrameRate(30);
                parameters.setPictureSize(960,480);
                parameters.setJpegQuality(100);*/

            List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
            Camera.Size previewSize = previewSizes.get(0);
            parameters.setPreviewSize(previewSize.width, previewSize.height);

            if (camera_id == EXT_CAM_ID)
                parameters.set("sensor-type",sensor_format);
            /**
             * support sensor type :
             * tvi-720p
             * tvi-1080p
             * ahd-720p
             * ahd-1080p
             * cvi-720p
             * cvi-1080p
             * ntsc
             */


            camera.setParameters(parameters);
            camera.setPreviewDisplay(mHolder);
            camera.startPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void refresh_camera(int camera_id, String sensor_format)
    {
        Log.d(TAG, "#refresh_camera() - camera_id("+camera_id+"), sensor_format("+sensor_format+")");
        if (mHolder.getSurface() == null)
            return;
        try {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
        catch (Exception e) {
            Log.d(TAG, "#refresh_camera() - " + e.getMessage());
        }

        try {
            create_camera(camera_id, sensor_format);

            camera.setPreviewDisplay(mHolder);
            camera.startPreview();
        }
        catch (Exception e) {
            Log.d(TAG, "#refresh_camera() - " + e.getMessage());
        }

    }
    public void refresh_camera(int camera_id)
    {
        Log.d(TAG, "#refresh_camera() - ");
        if (mHolder.getSurface() == null)
            return;
        try {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
        catch (Exception e) {
            Log.d(TAG, "#refresh_camera() - " + e.getMessage());
        }

        try {
            create_camera(camera_id, "ntsc");

            camera.setPreviewDisplay(mHolder);
            camera.startPreview();
        }
        catch (Exception e) {
            Log.d(TAG, "#refresh_camera() - " + e.getMessage());
        }
    }

    public void takecamera(View v) {
        if (camera != null) {
            switch (v.getId()) {
                case R.id.takepicture:
                    camera.takePicture(null, null, new MyPictureCallback());
                    break;
                case R.id.autofocus:
                    camera.autoFocus(null);
                    break;
                case R.id.back_cam_power:
                    back_cam_power();
                    break;
                case R.id.prev_back_cam:
                    refresh_camera(BACK_CAM_ID);
                    break;
                case R.id.prev_front_cam:
                    refresh_camera(FRONT_CAM_ID);
                    break;
                case R.id.prev_ext_cam_tvi720p:
                    refresh_camera(EXT_CAM_ID, "tvi-720p");
                    break;
                case R.id.prev_ext_cam_tvi1080p:
                    refresh_camera(EXT_CAM_ID, "tvi-1080p");
                    break;
                case R.id.prev_ext_cam_ahd720p:
                    refresh_camera(EXT_CAM_ID, "ahd-720p");
                    break;
                case R.id.prev_ext_cam_ahd1080p:
                    refresh_camera(EXT_CAM_ID, "ahd-1080p");
                    break;
                case R.id.prev_ext_cam_cvi720p:
                    refresh_camera(EXT_CAM_ID, "cvi-720p");
                    break;
                case R.id.prev_ext_cam_cvi1080p:
                    refresh_camera(EXT_CAM_ID, "cvi-1080p");
                    break;
                case R.id.prev_ext_cam_ntsc:
                    refresh_camera(EXT_CAM_ID, "ntsc");
                    break;
            }
        }
    }

    // the callback method after the photo is taken
    private final class MyPictureCallback implements Camera.PictureCallback {
        public void onPictureTaken(byte[] data, Camera camera) {
            try {
                File jpgFile = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis()+".jpg");
                FileOutputStream outStream = new FileOutputStream(jpgFile);
                outStream.write(data);
                outStream.close();
                Log.i(TAG, jpgFile.getAbsolutePath());
                Log.i(TAG, String.valueOf(jpgFile.exists()));
                camera.startPreview();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class SurfaceCallback implements SurfaceHolder.Callback {
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                create_camera(BACK_CAM_ID, "ntsc");
/*                camera = Camera.open(0);
                Camera.Parameters parameters = camera.getParameters();
                Log.i(TAG, parameters.flatten());
//                parameters.setPreviewSize(960,480);
//                parameters.setPreviewFrameRate(30);
//                parameters.setPictureSize(960,480);
//                parameters.setJpegQuality(100);

                List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
                Camera.Size previewSize = previewSizes.get(0);
                parameters.setPreviewSize(previewSize.width, previewSize.height);
*//**
 * support sensor type :
 * tvi-720p
 * tvi-1080p
 * ahd-720p
 * ahd-1080p
 * cvi-720p
 * cvi-1080p
 * ntsc
 *//*
//                parameters.set("sensor-type","ntsc");
                //parameters.set("sensor-type","cvi-1080p"); /////
                camera.setParameters(parameters);
                camera.setPreviewDisplay(holder);
                camera.startPreview();*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }
        public void surfaceDestroyed(SurfaceHolder holder) {
            if (camera != null) {
                camera.release();
                camera = null;
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            layout.setVisibility(ViewGroup.VISIBLE);
            return true;
        }
        return super.onTouchEvent(event);
    }


}
